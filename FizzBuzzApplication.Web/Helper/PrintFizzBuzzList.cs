﻿namespace FizzBuzzApplication.Web.Helper
{
    using System.Collections.Generic;
    using System.Web.Mvc;

    public static class PrintFizzBuzzList
    {
        public static MvcHtmlString PrintList(this HtmlHelper html, IEnumerable<string> fizzbuzzList)
        {
            TagBuilder tagOutterDiv = new TagBuilder("div");
            foreach (var item in fizzbuzzList)
            {
                TagBuilder tagInnerDiv = new TagBuilder("div");
                foreach (var value in item.Split(' '))
                {
                    TagBuilder tagSpan = new TagBuilder("Span");
                    tagSpan.AddCssClass(value);
                    tagSpan.SetInnerText(value + ' ');
                    tagInnerDiv.InnerHtml += tagSpan.ToString();
                }

                tagOutterDiv.InnerHtml += tagInnerDiv.ToString();
            }

            return new MvcHtmlString(tagOutterDiv.ToString());
        }
    }
}