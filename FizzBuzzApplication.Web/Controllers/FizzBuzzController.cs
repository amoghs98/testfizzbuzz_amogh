﻿namespace FizzBuzzApplication.Web.Controllers
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using FizzBuzzApplication.Service.Interface;
    using FizzBuzzApplication.Web.Helper;
    using FizzBuzzApplication.Web.Models;
    using PagedList;

    public class FizzBuzzController : Controller
    {
        private readonly IFizzBuzzService fizzBuzzService;

        public FizzBuzzController(IFizzBuzzService fizzBuzzService)
        {
            this.fizzBuzzService = fizzBuzzService;
        }

        public ActionResult Display()
        {
            return this.View("Display");
        }

        [HttpPost]
        public ActionResult DisplayList(FizzBuzzModel fizzbuzz)
        {
            if (ModelState.IsValid)
            {
                fizzbuzz.FizzBuzzList = this.GetFizzBuzzList(fizzbuzz.Input).ToPagedList(1, FizzBuzzWebConstants.PageSize);
            }

            return View("Display", fizzbuzz);
        }

        [HttpGet]
        public ActionResult GetPagedList(FizzBuzzModel fizzbuzz, int page)
        {
            var model = new FizzBuzzModel() { Input = fizzbuzz.Input };
            model.FizzBuzzList = this.GetFizzBuzzList(fizzbuzz.Input).ToPagedList(page, FizzBuzzWebConstants.PageSize); ;
            return this.View("Display", model);
        }

        private IEnumerable<string> GetFizzBuzzList(int number)
        {
            var resultList = this.fizzBuzzService.GetFizzBuzzList(number);
            return resultList;
        }
    }
}