﻿namespace FizzBuzzApplication.Service.Tests
{
    using System;
    using FizzBuzzApplication.Service.Interface;
    using FizzBuzzApplication.Service.Service;
    using NUnit.Framework;

    [TestFixture]
    internal class CheckDayTests
    {
        private ICheckDay checkDay;

        [SetUp]
        public void Initialise()
        {
            checkDay = new CheckDay(DayOfWeek.Wednesday);
        }

        [TestCase(DayOfWeek.Monday, false)]
        [TestCase(DayOfWeek.Wednesday, true)]
        public void SpecifiedDayCheck_Test(DayOfWeek today, bool expected)
        {
            // act
            var result = checkDay.SpecifiedDayCheck(today);

            // assert
            Assert.AreEqual(expected, result);
        }
    }
}