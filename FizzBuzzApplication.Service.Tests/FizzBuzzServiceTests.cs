﻿namespace FizzBuzzApplication.Service.Tests
{
    using System.Collections.Generic;
    using FizzBuzzApplication.Service.Helper;
    using FizzBuzzApplication.Service.Interface;
    using FizzBuzzApplication.Service.Service;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class FizzBuzzServiceTests
    {
        private Mock<IDivisionByNumber> mockDivisionByThree;
        private Mock<IDivisionByNumber> mockDivisionByFive;
        private IEnumerable<IDivisionByNumber> divisionByNumbers;
        private IFizzBuzzService fizzbuzzService;
        private int input;
        private List<string> testList;

        [SetUp]
        public void Initialize()
        {
            mockDivisionByThree = new Mock<IDivisionByNumber>();
            mockDivisionByFive = new Mock<IDivisionByNumber>();
            divisionByNumbers = new List<IDivisionByNumber>() { mockDivisionByThree.Object, mockDivisionByFive.Object };
            fizzbuzzService = new FizzBuzzService(divisionByNumbers);
            this.mockDivisionByThree.Setup(m => m.IsDivisible(It.Is<int>(x => x % 3 == 0))).Returns(true);
            this.mockDivisionByThree.Setup(m => m.IsDivisible(It.Is<int>(x => x % 3 != 0))).Returns(false);
            this.mockDivisionByFive.Setup(m => m.IsDivisible(It.Is<int>(x => x % 5 == 0))).Returns(true);
            this.mockDivisionByFive.Setup(m => m.IsDivisible(It.Is<int>(x => x % 5 != 0))).Returns(false);
            input = 15;
        }

        [Test]
        public void GetFizzBuzzList_Test_ForNotSpecifiedDay()
        {
            // arange
            testList = new List<string>() { "1", "2", "fizz", "4", "buzz", "fizz", "7", "8", "fizz", "buzz", "11", "fizz", "13", "14", "fizz buzz" };
            this.mockDivisionByThree.Setup(m => m.GetMessage()).Returns(FizzBuzzConstants.Fizz);
            this.mockDivisionByFive.Setup(m => m.GetMessage()).Returns(FizzBuzzConstants.Buzz);

            // act
            var result = this.fizzbuzzService.GetFizzBuzzList(this.input);

            // assert
            Assert.AreEqual(this.testList, result);
        }

        [Test]
        public void GetFizzBuzzList_Test_ForSpecifiedDay()
        {
            // arange
            testList = new List<string>() { "1", "2", "wizz", "4", "wuzz", "wizz", "7", "8", "wizz", "wuzz", "11", "wizz", "13", "14", "wizz wuzz" };
            this.mockDivisionByThree.Setup(m => m.GetMessage()).Returns(FizzBuzzConstants.Wizz);
            this.mockDivisionByFive.Setup(m => m.GetMessage()).Returns(FizzBuzzConstants.Wuzz);

            // act
            var result = this.fizzbuzzService.GetFizzBuzzList(this.input);

            // assert
            Assert.AreEqual(this.testList, result);
        }
    }
}
