﻿namespace FizzBuzzApplication.Service.Tests
{
    using System;
    using FizzBuzzApplication.Service.Interface;
    using FizzBuzzApplication.Service.Service;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class DivisibilityByFiveTests
    {
        private IDivisionByNumber divisibleByFive;
        private Mock<ICheckDay> mockcheckday;

        [SetUp]
        public void Initialise()
        {
            mockcheckday = new Mock<ICheckDay>();
            divisibleByFive = new DivisibilityByFive(mockcheckday.Object);
        }

        [TestCase(5, true)]
        [TestCase(7, false)]
        [TestCase(15, true)]
        public void IsDivisibleMethod_Test(int input, bool expected)
        {
            // act
            var result = divisibleByFive.IsDivisible(input);

            // assert
            Assert.AreEqual(expected, result);
        }

        [TestCase(5, "buzz")]
        [TestCase(15, "buzz")]
        public void GetMessageMethod_Test_For_Buzz(int input, string expected)
        {
            // act
            var result = divisibleByFive.GetMessage();

            // assert
            Assert.AreEqual(expected, result);
        }

        [TestCase(5, "wuzz")]
        [TestCase(15, "wuzz")]
        public void GetMessage_Test_For_Wuzz(int input, string expected)
        {
            // arrange
            this.mockcheckday.Setup(m => m.SpecifiedDayCheck(It.IsAny<DayOfWeek>())).Returns(true);

            // act
            var result = divisibleByFive.GetMessage();

                // assert
            Assert.AreEqual(expected, result);
        }
    }
}