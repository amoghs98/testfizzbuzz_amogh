﻿using System.Diagnostics;

namespace FizzBuzzApplication.Web.Tests.ControllerTests
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using FizzBuzzApplication.Service.Interface;
    using FizzBuzzApplication.Web.Controllers;
    using FizzBuzzApplication.Web.Models;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class FizzBuzzControllerTests
    {
        private FizzBuzzController controller;
        private FizzBuzzModel fizzbuzzModel;
        private Mock<IFizzBuzzService> mockFizzbuzzService;
        private List<string> testList;

        [SetUp]
        public void Initialize()
        {
            this.mockFizzbuzzService = new Mock<IFizzBuzzService>();
            this.controller = new FizzBuzzController(this.mockFizzbuzzService.Object);
            this.fizzbuzzModel = new FizzBuzzModel() { Input = 5 };
        }

        [Test]
        public void DisplayMethod_Test()
        {
            // act 
            ViewResult result = controller.Display() as ViewResult;

            // assert
            Assert.AreEqual("Display", result.ViewName);
        }

        [Test]
        public void DisplayListMethod_Test_For_FizzBuzz()
        {
            // arrange
            testList = new List<string>() { "1", "2", "fizz", "4", "buzz" };
            mockFizzbuzzService.Setup(m => m.GetFizzBuzzList(It.IsAny<int>())).Returns(testList);

            // act
            ViewResult result = controller.DisplayList(fizzbuzzModel) as ViewResult;
            FizzBuzzModel model = result.Model as FizzBuzzModel;

            // assert
            Assert.AreEqual(fizzbuzzModel, model);
            Assert.AreEqual("Display", result.ViewName);
            Assert.AreEqual(testList, model.FizzBuzzList);
        }

        [Test]
        public void DisplayListMethod_Test_For_WizzWuzz()
        {
            // arrange
            testList = new List<string>() { "1", "2", "wizz", "4", "wuzz" };
            mockFizzbuzzService.Setup(m => m.GetFizzBuzzList(It.IsAny<int>())).Returns(testList);

            // act
            ViewResult result = controller.DisplayList(fizzbuzzModel) as ViewResult;
            FizzBuzzModel model = result.Model as FizzBuzzModel;

            // assert
            Assert.AreEqual(fizzbuzzModel, model);
            Assert.AreEqual("Display", result.ViewName);
            Assert.AreEqual(testList, model.FizzBuzzList);
        }

        [Test]
        public void DisplayFizzBuzzList_Test_With_Model_Error()
        {
            // arrange
            var testFizzbuzz = new FizzBuzzModel() { Input = -2 };
            controller.ModelState.AddModelError("Input", "Enter an integer value between 1 and 1000");

            // act
            var result = controller.DisplayList(testFizzbuzz) as ViewResult;
            FizzBuzzModel model = result.Model as FizzBuzzModel;

            // assert
            Assert.AreEqual("Display", result.ViewName);
            Assert.IsNull(model.FizzBuzzList);
        }

        [Test]
        public void GetPagedList_Test()
        {
            // arrange
            int page = 1;
            var testFizzbuzz = new FizzBuzzModel()
            {
                Input = 15,
            };
            testList = new List<string>() { "1", "2", "fizz", "4", "buzz" };
            mockFizzbuzzService.Setup(p => p.GetFizzBuzzList(It.IsAny<int>())).Returns(testList);

            // act
            var result = controller.GetPagedList(testFizzbuzz, page) as ViewResult;
            FizzBuzzModel model = result.Model as FizzBuzzModel;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(model);
            Assert.AreEqual(result.ViewName, "Display");
            CollectionAssert.AreEquivalent(model.FizzBuzzList, testList);
        }
    }
}