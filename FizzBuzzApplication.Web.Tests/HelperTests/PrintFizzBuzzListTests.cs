﻿namespace FizzBuzzApplication.Web.Tests.HelperTests
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using FizzBuzzApplication.Web.Helper;
    using NUnit.Framework;

    [TestFixture]
    public class PrintFizzBuzzListTests
    {
        private string testString;
        private IEnumerable<string> testList;
        private HelperMock htmlHelperMock;

        [SetUp]
        public void Initialise()
        {
            this.htmlHelperMock = new HelperMock();
        }

        [Test]
        public void PrintListMethod_Test_For_FizzBuzz()
        {
            // arrange
            this.testString = "<div><div><Span class=\"1\">1 </Span></div><div><Span class=\"2\">2 </Span></div><div><Span class=\"fizz\">fizz </Span></div></div>";
            this.testList = new List<string>() { "1", "2", "fizz" };

            // act
            MvcHtmlString result = htmlHelperMock.CreateHtmlHelper(new ViewDataDictionary("Test")).PrintList(testList);

            // assert
            Assert.IsNotEmpty(result.ToString());
            Assert.AreEqual(this.testString, result.ToString());
        }

        [Test]
        public void PrintListMethod_Test_For_WizzWuzz()
        {
            // arrange
            this.testString = "<div><div><Span class=\"1\">1 </Span></div><div><Span class=\"2\">2 </Span></div><div><Span class=\"wizz\">wizz </Span></div></div>";
            this.testList = new List<string>() { "1", "2", "wizz" };

            // act
            MvcHtmlString result = htmlHelperMock.CreateHtmlHelper(new ViewDataDictionary("Test")).PrintList(testList);

            // assert
            Assert.IsNotEmpty(result.ToString());
            Assert.AreEqual(this.testString, result.ToString());
        }
    }
}