﻿namespace FizzBuzzApplication.Service.Service
{
    using System;
    using FizzBuzzApplication.Service.Helper;
    using FizzBuzzApplication.Service.Interface;

    public class DivisibilityByFive : IDivisionByNumber
    {
        private readonly ICheckDay checkDay;

        public DivisibilityByFive(ICheckDay checkDay)
        {
            this.checkDay = checkDay;
        }

        public bool IsDivisible(int input)
        {
            return input % 5 == 0;
        }

        public string GetMessage()
        {
            return checkDay.SpecifiedDayCheck(DateTime.Now.DayOfWeek) ? FizzBuzzConstants.Wuzz : FizzBuzzConstants.Buzz;
        }
    }
}