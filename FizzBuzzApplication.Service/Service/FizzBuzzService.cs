﻿namespace FizzBuzzApplication.Service.Service
{
    using System.Collections.Generic;
    using System.Linq;
    using FizzBuzzApplication.Service.Interface;

    public class FizzBuzzService : IFizzBuzzService
    {
        private readonly IList<string> fizzbuzzList;
        private readonly IEnumerable<IDivisionByNumber> divisionStrategies;

        public FizzBuzzService(IEnumerable<IDivisionByNumber> divisionStrategies)
        {
            this.fizzbuzzList = new List<string>();
            this.divisionStrategies = divisionStrategies;
        }

        public IEnumerable<string> GetFizzBuzzList(int input)
        {
            for (var index = 1; index <= input; index++)
            {
                var matchedStrategies = this.divisionStrategies.Where(m => m.IsDivisible(index)).ToList();
                fizzbuzzList.Add(matchedStrategies.Any() ? string.Join(" ", matchedStrategies.Select(m => m.GetMessage())) : index.ToString());
            }

            return fizzbuzzList;
        }
    }
}