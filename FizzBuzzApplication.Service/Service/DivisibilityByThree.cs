﻿namespace FizzBuzzApplication.Service.Service
{
    using System;
    using FizzBuzzApplication.Service.Helper;
    using FizzBuzzApplication.Service.Interface;

    public class DivisibilityByThree : IDivisionByNumber
    {
        private readonly ICheckDay checkDay;

        public DivisibilityByThree(ICheckDay checkDay)
        {
            this.checkDay = checkDay;
        }

        public bool IsDivisible(int input)
        {
            return input % 3 == 0;
        }

        public string GetMessage()
        {
            return checkDay.SpecifiedDayCheck(DateTime.Now.DayOfWeek) ? FizzBuzzConstants.Wizz : FizzBuzzConstants.Fizz;
        }
    }
}