﻿namespace FizzBuzzApplication.Service.Interface
{
    using System;

    public interface ICheckDay
    {
        bool SpecifiedDayCheck(DayOfWeek today);
    }
}