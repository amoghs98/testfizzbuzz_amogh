﻿namespace FizzBuzzApplication.Service.Interface
{
    using System.Collections.Generic;

    public interface IFizzBuzzService
    {
        IEnumerable<string> GetFizzBuzzList(int input);
    }
}
