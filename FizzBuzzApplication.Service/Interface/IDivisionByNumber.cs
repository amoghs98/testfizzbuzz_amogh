﻿namespace FizzBuzzApplication.Service.Interface
{
    public interface IDivisionByNumber
    {
        bool IsDivisible(int input);

        string GetMessage();
    }
}
